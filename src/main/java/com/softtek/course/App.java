package com.softtek.course;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "Hello World!" );
    }

    public static void hola()
    {
        System.out.println( "hola!" );
    }

    public static void adios()
    {
        System.out.println( "adios!" );
    }
}
